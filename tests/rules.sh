source helpers.sh

function test_admin {
    INTERFACE=$1
    expect_inf_to_reach_host $INTERFACE 192.168.178.1 # gateway
    expect_inf_to_reach_http $INTERFACE http://192.168.178.1
}

function test_restricted {
    INTERFACE=$1
    expect_inf_to_reach_host $INTERFACE 192.168.100.1 # gateway
    expect_inf_to_reach_host $INTERFACE 192.168.178.1 # router admin
    expect_inf_to_reach_host $INTERFACE 208.67.222.222
    expect_inf_to_reach_host $INTERFACE 192.168.100.2 # kanthaus-server
    expect_inf_to_reach_host $INTERFACE kanthaus-server
    expect_inf_to_reach_host $INTERFACE google.de
    expect_inf_to_reach_host $INTERFACE 192.168.4.2 # printer @ kanthaus-server
    expect_inf_to_reach_host $INTERFACE 192.168.4.153 # printer direct
    expect_inf_to_not_reach_host $INTERFACE 192.168.102.2 # kanthaus-server in UNSECURE net
    expect_inf_to_not_reach_host $INTERFACE 192.168.5.10 # SMA
    expect_inf_to_not_reach_host $INTERFACE 192.168.5.11 # SMA
    expect_inf_to_not_reach_host $INTERFACE 192.168.200.1 # fritz box
    expect_inf_to_reach_http $INTERFACE http://kanthaus-server
    expect_inf_to_reach_http $INTERFACE http://premiumize.im.kanthaus.online
    expect_inf_to_not_reach_http $INTERFACE http://192.168.178.1
    expect_inf_to_not_reach_http $INTERFACE http://192.168.178.2
    expect_inf_to_reach_udp $INTERFACE 192.168.100.2 631
    expect_inf_to_reach_host $INTERFACE ipv6.google.com
}

function test_insecure {
    INTERFACE=$1
    expect_inf_to_reach_host $INTERFACE 192.168.102.2 # gateway
    expect_inf_to_reach_host $INTERFACE 208.67.222.222
    expect_inf_to_reach_host $INTERFACE google.de
    expect_inf_to_reach_host $INTERFACE kanthaus-server
    expect_inf_to_reach_host $INTERFACE 192.168.100.2
    # TODO: expect_inf_to_not_reach_host 192.168.100.x/32 (except kh-server)
    expect_inf_to_reach_host $INTERFACE 192.168.4.2 # printer @ kanthaus-server
    expect_inf_to_reach_host $INTERFACE 192.168.4.153 # printer direct
    expect_inf_to_not_reach_http $INTERFACE http://192.168.178.1
    expect_inf_to_not_reach_http $INTERFACE http://192.168.178.2
    expect_inf_to_not_reach_host $INTERFACE 192.168.5.10 # SMA
    expect_inf_to_not_reach_host $INTERFACE 192.168.5.11 # SMA
    expect_inf_to_not_reach_host $INTERFACE 192.168.200.1 # fritz box
    expect_inf_to_reach_http $INTERFACE http://premiumize.im.kanthaus.online
    expect_inf_to_reach_udp $INTERFACE 192.168.4.2 631
    expect_inf_to_reach_host $INTERFACE ipv6.google.com
}


function test_vpn {
    INTERFACE=$1
    expect_inf_to_reach_host $INTERFACE 192.168.101.1 # gateway
    expect_inf_to_reach_host $INTERFACE 208.67.222.222
    expect_inf_to_reach_host $INTERFACE google.de
    expect_inf_to_not_reach_host $INTERFACE 192.168.4.2 # printer @ kanthaus-server
    expect_inf_to_not_reach_host $INTERFACE 192.168.4.153 # printer direct
    expect_inf_to_not_reach_host $INTERFACE 192.168.100.2
    expect_inf_to_not_reach_host $INTERFACE kanthaus-server
    expect_inf_to_not_reach_host $INTERFACE 192.168.102.2
    expect_inf_to_not_reach_host $INTERFACE 192.168.5.10 # SMA
    expect_inf_to_not_reach_host $INTERFACE 192.168.5.11 # SMA
    expect_inf_to_not_reach_host $INTERFACE 192.168.200.1 # fritz box
    expect_inf_to_not_reach_http $INTERFACE http://192.168.178.1
    expect_inf_to_not_reach_http $INTERFACE http://192.168.178.2
}

function test_printer {
    INTERFACE=$1
    expect_inf_to_reach_host $INTERFACE 192.168.4.2
    expect_inf_to_not_reach_host $INTERFACE 208.67.222.222
    expect_inf_to_not_reach_host $INTERFACE 192.168.100.2
    expect_inf_to_not_reach_host $INTERFACE 192.168.102.2
    expect_inf_to_not_reach_host $INTERFACE 192.168.178.2
    expect_inf_to_not_reach_host $INTERFACE 192.168.200.1 # fritz box
    expect_inf_to_not_reach_http $INTERFACE http://192.168.178.1
    # TODO: disallow DNS
}

function test_iot {
    INTERFACE=$1
    expect_inf_to_reach_host $INTERFACE 192.168.5.2
    expect_inf_to_not_reach_host $INTERFACE 208.67.222.222
    expect_inf_to_not_reach_host $INTERFACE 192.168.100.2
    expect_inf_to_not_reach_host $INTERFACE 192.168.102.2
    expect_inf_to_not_reach_host $INTERFACE 192.168.178.2
    expect_inf_to_not_reach_http $INTERFACE http://192.168.178.1
}