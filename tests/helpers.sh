#!/bin/bash

TICK="\e[32m✓\e[0m"
CROSS="\e[31m⛌\e[0m"


function ensure_vlan {
    INF=$1
    VID=$2
    echo -e "[$TICK][$INF.$VID] create"
    ip link add link $INF name $INF.$VID type vlan id $VID 2>&1 | grep -v "File exists"  || true
    ip link set $INF.$VID up || true
}
function test_dhcp {
    response=$(nmap --script broadcast-dhcp-discover -e $1 --script-args=broadcast-dhcp-discover.timeout=2 2>&1)
    echo $response  | grep Response > /dev/null \
        && echo -e "[$TICK][$1] DHCP ok" \
        || (echo -e "[$CROSS][$1] DHCP failed!"; echo $response; exit 1)
    dhclient $1 2> /dev/null
}

function get_inf_ip {
    ip addr show $1  | grep 'inet ' | awk '{print $2}' | awk -F/ '{print $1 }'    
}
function static_ip {
    ip addr replace $2 dev $1
}

function expect_inf_to_reach_host {
    ping -q -I $1 -c 1 -W 2 $2 > /dev/null 2> /dev/null \
        && echo -e "[$TICK] $1 -> $2: reachable as expected" \
        || echo -e "[$CROSS] $1 -> $2: ERROR: not reachable"
}
function expect_inf_to_not_reach_host {
    ping -q -I $1 -c 1 -W 1 $2 > /dev/null 2> /dev/null \
        && echo -e "[$CROSS] $1 -> $2: ERROR: reachable, but should not!" \
        || echo -e "[$TICK] $1 -> $2: not reachable as expected"
}

function expect_inf_to_reach_http {
    curl -s  --connect-timeout 1 --interface $1 $2 >/dev/null \
        && echo -e "[$TICK] $1 -> $2: reachable as expected" \
        || echo -e "[$CROSS] $1 -> $2: ERROR: not reachable"
}
function expect_inf_to_not_reach_http {
    curl -s --connect-timeout 1 --interface $1 $2 >/dev/null \
        && echo -e "[$CROSS] $1 -> $2: ERROR: reachable, but should not!" \
        || echo -e "[$TICK] $1 -> $2: not reachable as expected"
}

function expect_inf_to_reach_udp {
    SRC_IP=$(get_inf_ip $1)
    IP=$2
    PORT=$3
    netcat -s $SRC_IP -z -u -n -v $IP $PORT 2>&1 | grep succeeded > /dev/null \
        && echo -e "[$TICK] $1 -> $IP:$PORT/udp: reachable as expected" \
        || echo -e "[$CROSS] $1 -> $IP:$PORT/udp: ERROR: not reachable"
}