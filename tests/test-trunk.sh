#!/bin/bash

set -e

source rules.sh
source helpers.sh

INF=$(ip link | grep --only-matching -E ' (enp|ens)[^\.:]+' | head -n 1)

echo using device $INF

ensure_vlan $INF 4
ensure_vlan $INF 5
ensure_vlan $INF 100
ensure_vlan $INF 101
ensure_vlan $INF 102

static_ip $INF.4 192.168.4.230/24
static_ip $INF.5 192.168.5.230/24

test_dhcp $INF.5
test_dhcp $INF.100
test_dhcp $INF.101
test_dhcp $INF.102


echo ""
echo "# ADMIN"
test_admin $INF

echo ""
echo "# RESTRICTED"
test_restricted $INF.100

echo ""
echo "# VPN"
test_vpn $INF.101

echo ""
echo "# UNSECURE"
test_insecure $INF.102


echo ""
echo "# PRINTER"
test_printer $INF.4

echo ""
echo "# IOT"
test_iot $INF.5