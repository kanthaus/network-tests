#!/bin/bash

set -e

source rules.sh
source helpers.sh

echo ""
echo "# ADMIN"
nmcli dev wifi connect kh-admin && sleep 1s
test_admin wlan0

echo ""
echo "# RESTRICTED"
nmcli dev wifi connect kanthaus && sleep 1s
test_restricted wlan0

echo ""
echo "# VPN"
nmcli dev wifi connect kanthaus-gast && sleep 1s
test_vpn wlan0

echo ""
echo "# INSECURE"
nmcli dev wifi connect kanthaus-insecure && sleep 1s
test_insecure wlan0


echo ""
echo "# IOT"
nmcli dev wifi connect kh-iot && sleep 1s
test_iot wlan0
